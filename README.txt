Gitpal.org
==========
Gitpal module contains features to export and import entities as serialized JSON
and store them in a local git repository for version control purposes.

Git comes with all the necessary methods to keep control of content and allows
us to work with remote repositories. Making this functionality available to the
Drupal Entity API and user interface will enable us to maintain content on
several branches, same like we do with source code. Of course there are several
challenges to solve, but a reliable implementation will allow us to provide
further modules to enable automation, deployment and merging of content without
loosing track of the history of it - even on distributed sites!

This means we could provide enterprise ready version control for content by
combining two of the most popular open source projects in the history of the
web.

Next steps:

- Find and excite volunteers for this project who like to contribute!
- Discuss the best approach for a Git connector and the usage of the Entity API
  to enable basic features (like clone, pull, push, checkout, commit)
- Define the basic UI elements, like connecting to a remote repository and
  extend the existing revision architecture with Git methods
- Identify the best structure for content to be stored it in a Git repository
  and make it accessible (in terms of listing, searching and so on)
- Think about content automation possibilities, like automatic deployment to
  connected sites, distributed work flows and only applying change sets to keep
  local modifications (in terms of collaboration)
- This project is inspired by conversations at the Drupal Con Barcelona 2015 and
  is looking for co-maintainers who like to leverage the way how we work with
  content in the future. Any feedback is really appreciated, because this is a
  project which can't be handled by a single persons alone! Right now it's just
  an idea, but together we could prove the value of it and finally make content
  versioning really reliable!

Get in contact: contact@gitpal.org
Slack Channel:  https://drupal.slack.com/messages/gitpal/

Gitpal.org has no official logo yet. No trademark is registered, the trademark
of the Drupal logo used in the mashup belongs to Drupal of course.

This module is a sandbox module and it's purpose is a proof of concept.
It's not ready for production sites yet.


Install
=======
1. Configure the private files directory for Drupal and make it writeable,
   Gitpal will write its repository into it.
2. Gitpal requires cpliakas/git-wrapper, which can be installed via composer.
   Ensure git (https://git-scm.com/) is installed properly on your server.
3. Ensure that your Drupal composer.json file, includes modules composer files
   as well by adding "modules/*/composer.json" to the "merge-plugin" block:

    "extra": {
        "merge-plugin": {
            "include": [
                "core/composer.json",
                "modules/*/composer.json"
            ],

4. Run composer and rebuild your caches.
5. Create a new node and a new revision to see Gitpal working in the background.
   You will find a Gitpal tab, beside the revisions tab in your canonical menu.

Hint: Per default, Gitpal stores any node revision in its local repository.
Disabling the "Create new revision" will update the files, but not commit them.


Author/Maintainers
==================
- Rouven Volk <r-volk> http://www.rvolk.de
- Michael Miles <mikemiles86> http://www.mike-miles.com

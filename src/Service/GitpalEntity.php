<?php
/**
 * @file
 * Contains \Drupal\gitpal\Service|GitpalEntity.
 */
namespace Drupal\gitpal\Service;

class GitpalEntity extends Gitpal {

  /**
   * Performs git commit for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *  Entity to commit.
   *
   * @return bool
   *   Status of commit.
   */
  public function entityCommit(\Drupal\Core\Entity\EntityInterface $entity) {
    // Get entity file uri.
    $filename = $this->getEntityFileUri($entity->getEntityTypeId(), $entity->uuid());

    // Get default commit message.
    $message = 'Gitpal insert';
    // Get message from entity log if possible.
    if (isset($entity->revision_log->value) && strlen($entity->revision_log->value)) {
      $message = (string) $entity->revision_log->value;
    }
    // Else does the file exist?
    elseif (file_exists($this->fileSystem->realpath($filename))) {
      // Set update message.
      $message = 'Gitpal update';
    }

    // Make the commit.
    return $this->commit($message, [$filename => $entity]);
  }

  /**
   * Retrieve entity from git repo.
   *
   * @param string $type
   *   Entity type.
   * @param string $uuid
   *   Entity uuid.
   *
   * @return bool|mixed
   *   Boolean FALSE if not loaded, or entity object.
   */
  public function loadEntity($type, $uuid) {
    $entity = FALSE;
    // Able to retrieve content?
    if ($content = $this->loadFile($this->getEntityFileUri($type, $uuid))) {
      // Need to get the entity type definition.
      if ($entity_type = \Drupal::entityTypeManager()->getDefinition($type)) {
        // Deserialize the retrieved file content based on entity class.
        $entity = $this->deserialize($content, $entity_type->getClass(), 'json');
      }
    }

    return $entity;
  }

  /**
   * Build the Uri to an entity file in the repo.
   *
   * @param string $type
   *   The entity type.
   * @param string $uuid
   *   The entity UUID.
   *
   * @return string
   *   Filepath to entity .json file in repo.
   */
  public function getEntityFileUri($type, $uuid) {

    return sprintf(
      '%s/%s/%s.json',
      $this->git_directory,
      $type,
      $uuid
    );

  }

  /**
   * Retreive just the filename for the repo entity file.
   *
   * @param string $type
   *   The entity type.
   * @param $uuid
   *   The entity uuid.
   *
   * @return string
   *   The entity filename.
   */
  public function getEntityFilename($type, $uuid) {
    return $this->fileSystem->basename($this->getEntityFileUri($type, $uuid));
  }
}

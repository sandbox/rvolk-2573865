<?php
/**
 * @file
 * Contains \Drupal\gitpal\Service\Gitpal.
 */


namespace Drupal\gitpal\Service;

use Drupal\gitpal\GitpalInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Drupal\serialization\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\serialization\Normalizer\ComplexDataNormalizer;
use Drupal\serialization\Normalizer\ListNormalizer;
use Drupal\serialization\Normalizer\TypedDataNormalizer;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;

/**
 * Class Gitpal
 * @package Drupal\gitpal\Service
 */
class Gitpal implements GitpalInterface  {

  /** @var \Drupal\Core\File\FileSystemInterface  */
  protected $fileSystem;
  /** @var \GitWrapper\GitWrapper  */
  protected $gitWrapper;
  /** @var \Symfony\Component\Serializer\Serializer  */
  protected $serializer;
  /** @var string  */
  protected $git_directory;

  /**
   * Gitpal constructor.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \GitWrapper\GitWrapper                $gitWrapper
   */
  public function __construct(\Drupal\Core\File\FileSystemInterface $fileSystem, \GitWrapper\GitWrapper $gitWrapper) {

    $this->fileSystem = $fileSystem;
    $this->gitWrapper = $gitWrapper;

    // @TODO: This needs to be reworked due to allow for dependency injection.
    $normalizers = [new ComplexDataNormalizer(), new ListNormalizer(), new TypedDataNormalizer()];
    $encoders = [new JsonEncoder(new JsonEncode(JSON_PRETTY_PRINT))];

    $this->serializer = new Serializer($normalizers, $encoders);

    //@todo: change this to a config variable?
    $this->git_directory = 'private://gitpal';

    //Initialize Git repository.
    $this->gitInit();
  }

  /**
   * Initializes Git Repository to store data.
   */
  public function gitInit() {

    $git = $this->getWorkingCopy();

    try {
      $git->status();
    } catch (\GitWrapper\GitException $e) {
      $git->init();
      // quickly fake user
      $config = \Drupal::config('system.site');
      $git->config('user.name', $config->get('name'));
      $git->config('user.email', $config->get('mail'));
      // create some fake read me, warn the user of gitpal's activity
      touch($this->fileSystem->realpath($this->git_directory) . '/README.md');
      $git->add('.');
      $git->commit('Initial Commit');
    }
  }

  /**
   * Helper method for retrieving the git directory.
   *
   * @return string
   *   The string vlaue of the git repository.
   */
  public function getGitDirectory() {
    return $this->git_directory;
  }

  /**
   * {@inheritdoc}
   */
  public function commit($message, array $files = []) {
    // Retrieve the working git directory.
    $git = $this->getWorkingCopy();

    // Are there files to add?
    if (!empty($files)) {
      // Loop through all the files.
      foreach ($files as $filename => $entity) {
        // Attempt to save file.
        if ($this->saveFile($filename, $entity)) {
          // add the file to Git repo. Need to use the realpath.
          $git->add($this->fileSystem->realpath($filename));
        }
        else {
          // @todo: Need to set an error message. Watchdog?
        }
      }
    }
    // Create git commit with message.
    return $git->commit($message);
  }

  /**
   * {@inheritdoc}
   */
  public function saveFile($filename, $content) {
    // Get directory.
    $directory = $this->fileSystem->dirname($filename);
    // Make sure directory exists.
    $this->checkDirectory($directory);
    // Serialize the data
    $data = $this->serialize($content);
    // Save the data
    return file_unmanaged_save_data($data, $filename, FILE_EXISTS_REPLACE);
  }

  /**
   * Checks for existence of directory and creates it if it does not exist.
   *
   * @param string $directory
   *   Name of the directory
   */
  private function checkDirectory($directory) {
    // Get realpath to the directory
    $directory = $this->fileSystem->realpath($directory);
    // Does directory not exist?
    if (!file_exists($directory)) {
      // Create new directory.
      $this->fileSystem->mkdir($directory, NULL, TRUE);
    }
  }

  /**
   * Retrieve the serlialized content of a file.
   * @param $filename
   * @return string
   */
  public function loadFile($filename) {
    return file_get_contents($this->fileSystem->realpath($filename));
  }

  /**
   * {@inheritdoc}
   */
  public function serialize($data) {
    return $this->serializer->serialize($data, 'json');
  }

  /**
   * {@inheritdoc}
   */
  public function deserialize($data, $type) {
    $serializer = \Drupal::service('serializer');
    return $serializer->deserialize($data, $type, 'json');
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkingCopy() {
    $this->checkDirectory($this->git_directory);
    // Get realpath.
    $realpath = $this->fileSystem->realpath($this->git_directory);
    // Initialize Git.
    $git = $this->gitWrapper->WorkingCopy($realpath);
    // Return git working copy.
    return $git;
  }

  /**
   * Returns the git status of the repo or a particular file in repo.
   *
   * @param string|boolean $file
   *    The name of the file to check.
   *
   * @return string
   */
  public function getStatus($file = FALSE) {
    $git = $this->getWorkingCopy();
    $status = '';
    if ($file) {
      $realfile = $this->getRealPath($file);
      if (file_exists($realfile)) {
        $status = $git->status($realfile,'--short')->getOutput();
      }
    }
    else {
      $status = $git->status()->getOutput();
    }

    return $status;
  }

  /**
   * Retrieves the diff status for a particular file in repo
   *
   * @param string $file
   *   The filename to check.
   *
   * @return mixed
   */
  public function getDiff($file) {
    $git = $this->getWorkingCopy();
    $diff = '';
    $realfile = $this->getRealPath($file);
    if (file_exists($realfile)) {
      $diff = $git->diff($file)->getOutput();
    }
    return $diff;
  }

  /**
   * Retrieves the diff status for a particular commit in repo
   *
   * @param string $commit
   *   The commit to check.
   *
   * @return mixed
   */
  public function getDiffCommit($commit, $html=FALSE) {
    $git = $this->getWorkingCopy();
    if ($html) {
      $ansi = $git->diff($commit.'^','--color-words')->getOutput();
      $converter = new AnsiToHtmlConverter();
      return $converter->convert($ansi);
    }
    else {
      return $git->diff($commit.'^')->getOutput();
    }
  }

  /**
   * Return current git log messages.
   *
   * @return mixed
   */
  public function getLog($file = FALSE, $as_array = FALSE) {
    $git = $this->getWorkingCopy();
    $log = '';
    if ($file) {
      $realfile = $this->getRealPath($file);
      if (file_exists($realfile)) {
        $log = $git->log($realfile)->getOutput();
      }
    }
    else {
      $log = $git->log()->getOutput();
    }
    if ($as_array) {
      $log = preg_split('/^(?=commit \w+)/im', $log, NULL, PREG_SPLIT_NO_EMPTY);
    }
    return $log;
  }

  /**
   * Returns the actual path to a file.
   *
   * @param string $file
   *   Name of file.
   *
   * @return FALSE|string
   *   Actual filepath or FALSE if it doesnot exist.
   */
  public function getRealPath($file) {
    return $this->fileSystem->realpath($file);
  }

}

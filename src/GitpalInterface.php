<?php
/**
 * @file
 * Contains \Drupal\gitpal\GitpalInterface.
 */
namespace Drupal\gitpal;

interface GitpalInterface {

  /**
   * Creates a git commit, along with adding files.
   *
   * @param string $message
   *   The commit message
   * @param array $files
   *   Array of files to commit to repo. Structured as:
   *    filename => file_contents.
   *
   * @return boolean
   *   Boolean TRUE or FALSE.
   */
  public function commit($message, array $files = []);

  /**
   * Saves a file into the git repo.
   *
   * @param string $filename
   *   The name of the file to save.
   * @param mixed $content
   *   Content to store int he file, unserialized.
   *
   * @return bool|null
   *   Boolean TRUE or FALSE based on success of saving file.
   */
  public function saveFile($filename, $content);

  /**
   * Serializes file content
   *
   * @param mixed $data
   *   Content to serialize
   *
   * @return mixed
   *   Serialized content
   */
  public function serialize($data);

  /**
   * Unserializes content based on type.
   *
   * @param string $data
   *   Serialized data
   *
   * @param string $type
   *   Type to deserialize.
   *
   * @return mixed
   *   Deserialized content.
   */
  public function deserialize($data, $type);

  /**
   * Retrieves GitWrapper object for working git repo.
   *
   * @return \GitWrapper\GitWorkingCopy
   */
  public function getWorkingCopy();

}

<?php

/**
 * @file
 * Contains \Drupal\gitpal\Form\GitpalForm.
 */

namespace Drupal\gitpal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class GitpalForm extends ConfigFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'gitpal_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor
    $form = parent::buildForm($form, $form_state);
    // Default settings
    $config = $this->config('gitpal.settings');

    $form['test'] = array(
      '#type' => 'container',
      '#markup' => \Drupal::l(
                    t('Gitpal test'),
                    Url::fromRoute('gitpal.test')
                  )
    );

    $form['init'] = array(
      '#type' => 'container',
      '#markup' => \Drupal::l(
                    t('Gitpal init'),
                    Url::fromRoute('gitpal.init')
                  )
    );


    /*
    $form['git_exec'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Git runtime executive'),
      '#default_value' => $config->get('gitpal.git_exec'),
      '#description' => $this->t('Enter the path of your git executive file.'),
    );
    */

    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('gitpal.settings');
    $config->set('gitpal.git_exec', $form_state->getValue('git_exec'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}.
   */
  protected function getEditableConfigNames() {
    return [
      'gitpal.settings',
    ];
  }

}

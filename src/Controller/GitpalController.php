<?php
/**
 * @file
 * Contains \Drupal\gitpal\Controller\GitpalController.
 */

namespace Drupal\gitpal\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;

class GitpalController extends ControllerBase {
  public function test() {
    return array(
        '#type' => 'markup',
        '#markup' => gitpal_test(),
    );
  }

  public function init() {
    return array(
        '#type' => 'markup',
        '#plain_text' => gitpal_init(),
    );
  }

  /**
   * Import UUID entity
   *
   * @param string $type
   * @param string $uuid
   */
  public function import($type, $uuid) {
    return array(
        '#type' => 'markup',
        '#markup' => gitpal_import_entity($type, $uuid),
    );
  }

  /**
   * Gitpal Diff page
   *
   * @param string $commit
   */
  public function diff($commit) {
    $gitpal = \Drupal::service('gitpal.service');

    $build = array(
        '#title' => $this->t('Gitpal Diff @commit', array('@commit'=>$commit)),
        '#type' => 'inline_template',
        '#template' => '<pre style="background-color: black; overflow: auto; padding: 10px 15px;">{{ diff | raw }}</pre>',
        '#context' => [
          'diff' => $gitpal->getDiffCommit($commit, TRUE)
        ]
    );

    return $build;
  }


  /**
   * Generates an overview of gitpal data about this node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function entityOverview(NodeInterface $node) {
    $account = $this->currentUser();
    // TODO: multilang support for gitpal storage
    $langcode = $node->language()->getId();
    $langname = $node->language()->getName();
    $languages = $node->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $node_storage = $this->entityManager()->getStorage('node');
    $type = $node->getType();

    $build = array();
    $build['#title'] = $has_translations ? $this->t('@langname Gitpal overview for %title', ['@langname' => $langname, '%title' => $node->label()]) : $this->t('Gitpal overview for %title', ['%title' => $node->label()]);

    $gitpal = \Drupal::service('gitpal.entity');

    // Retrieve gitpal filenURI for this entity.
    $file = $gitpal->getEntityFileUri($node->getEntityTypeId(), $node->uuid());
    if ($file && !file_exists($gitpal->getRealPath($file))) {
      drupal_set_message(t("Gitpal @file doesn't exists!", array('@file'=>$file)), 'warning');
      return $build;
    }

    if ($status = $gitpal->getStatus($file)) {
      drupal_set_message(t("Gitpal @file status:<pre>@status</pre>",
        array(
          '@file'=> $file,
          '@status' => $status
        )),
        'warning'
      );
    }

    $diff = $gitpal->getDiff($gitpal->getRealPath($file));
    if (strlen($diff)) {
      drupal_set_message(t("Gitpal @file diff:<pre>@diff</pre>",
        array(
          '@file'=> $file,
          '@diff'=>$diff
        )),
        'warning');
    }

    $header = array($this->t('Revision'), $this->t('Operations'));

    $rows = array();
    foreach ($gitpal->getLog($file, true) as $log) {
        preg_match('/^commit \b([0-9a-f]{5,40})\b/', $log, $m);
        $commit = $m[1];

        $row = [];
        $row[] = [
          'data' => [
            '#prefix' => '<pre>',
            '#markup' => $log,
            '#suffix' => '</pre>',
          ],
        ];

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => [
              'diff' => [
                'title' => $this->t('Diff'),
                'url' => Url::fromRoute('gitpal.diff', ['commit' => $commit]),
              ],
            ],
          ],
        ];
        $rows[] = $row;

    }

    $build['node_revisions_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attached' => array(
        'library' => array('node/drupal.node.admin'),
      ),
    );

    return $build;
  }

}
